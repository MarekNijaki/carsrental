﻿using System;
using CarsRental.Models;
using System.Collections.Generic;

namespace CarsRental.MixedModels
{
    /// <summary>
    ///   Mixed model for creating or editing car.
    /// </summary>
    public class CreateOrEditCar
    {
        #region Properties

        /// <summary>
        ///   Car.
        /// </summary>
        public Car Car { get; set; }

        /// <summary>
        ///   List of car types.
        /// </summary>
        public IEnumerable<CarType> CarTypes { get; set; }

        /// <summary>
        ///   Title of form.
        /// </summary>
        public string Title
        {
            get
            {
                if ((Car != null) && (Car.Id != 0))
                {
                    return "Edit car";
                }

                return "Create car";
            }
        }

        #endregion
    }
}