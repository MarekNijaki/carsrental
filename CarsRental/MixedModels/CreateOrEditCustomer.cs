﻿using CarsRental.Models;
using System.Collections.Generic;

namespace CarsRental.MixedModels
{
    /// <summary>
    ///   Mixed model for creating or editing customer.
    /// </summary>
    public class CreateOrEditCustomer
    {
        #region Properties

        /// <summary>
        ///   Customer.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        ///   List of subscription types.
        /// </summary>
        public IEnumerable<SubscriptionType> SubscriptionTypes { get; set; }

        /// <summary>
        ///   Title of form.
        /// </summary>
        public string Title
        {
            get
            {
                if ((Customer != null) && (Customer.Id != 0))
                {
                    return "Edit customer";
                }

                return "Create customer";
            }
        }

        #endregion
    }
}