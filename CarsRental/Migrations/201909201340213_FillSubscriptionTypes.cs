namespace CarsRental.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class FillSubscriptionTypes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT " +
                "  INTO SubscriptionTypes (Name, Fee, DiscountPercentage, DurationInMonths)" +
                "  VALUES ('Single payment',0,0,0)");
            Sql("INSERT " +
                "  INTO SubscriptionTypes (Name, Fee, DiscountPercentage, DurationInMonths)" +
                "  VALUES ('Monthly',20,10,1)");
            Sql("INSERT " +
                "  INTO SubscriptionTypes (Name, Fee, DiscountPercentage, DurationInMonths)" +
                "  VALUES ('Quarterly',60,20,3)");
            Sql("INSERT " +
                "  INTO SubscriptionTypes (Name, Fee, DiscountPercentage, DurationInMonths)" +
                "  VALUES ('Annual',240,30,12)");
        }
        
        public override void Down()
        {

        }
    }
}
