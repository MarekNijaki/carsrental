namespace CarsRental.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class FillCarTypes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT" +
                "  INTO CarTypes (Name)" +
                "  VALUES ('Passenger car')");
            Sql("INSERT" +
                "  INTO CarTypes (Name)" +
                "  VALUES ('Delivery truck')");
            Sql("INSERT" +
                "  INTO CarTypes (Name)" +
                "  VALUES ('18-wheeler')");
            Sql("INSERT" +
                "  INTO CarTypes (Name)" +
                "  VALUES ('Autobus')");
        }

        public override void Down()
        {

        }
    }
}
