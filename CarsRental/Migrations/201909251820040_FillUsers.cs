namespace CarsRental.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FillUsers : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT" +
                "  INTO [dbo].[AspNetUsers]" +
                "  ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], " +
                "   [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], " +
                "   [AccessFailedCount], [UserName]) " +
                "   VALUES(N'9231786b-f5b5-496e-bc4a-86b17f464ff0', N'guest@carsrental.com', 0, " +
                "   N'AACssTkBXWdSPPSR7LqBiRDJUwdz7CRx7Q10AbsdJmhEy0fFeF9FKtJFYKsQBDKnyQ==', " +
                "   N'5d88bbbc-bd2f-4ba1-a980-ac0de1d0810b', NULL, 0, 0, NULL, 1, 0, N'guest@carsrental.com')");

            Sql("INSERT" +
                "  INTO [dbo].[AspNetUsers]" +
                "  ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], " +
                "   [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], " +
                "   [AccessFailedCount], [UserName]) " +
                "   VALUES(N'a904824a-13bd-48b6-8de9-7ac1ce7b0407', N'admin@carsrental.com', 0, " +
                "   N'AK6Ufp8dqMWsEMETBQ8BiB6thEiqLjhec+wdX7tdwfYYxriYsE9Bj3vAwkbWq4PaUQ==', " +
                "   N'f015bd49-950f-4c42-94df-1906fe4cb6af', NULL, 0, 0, NULL, 1, 0, N'admin@carsrental.com')");

            Sql("INSERT " +
                "  INTO[dbo].[AspNetRoles]" +
                "  ([Id], [Name]) " +
                "  VALUES(N'5714d205-bf22-49fd-8a5c-37a13f92c5e5', N'Admin')");

            Sql("INSERT " +
                "  INTO[dbo].[AspNetUserRoles]" +
                "  ([UserId], [RoleId])" +
                "  VALUES(N'a904824a-13bd-48b6-8de9-7ac1ce7b0407', N'5714d205-bf22-49fd-8a5c-37a13f92c5e5')");
        }

        public override void Down()
        {

        }
    }
}
