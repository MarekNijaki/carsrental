﻿using System.ComponentModel.DataAnnotations;

namespace CarsRental.DTOs
{
    /// <summary>
    ///   Type of subscription.
    /// </summary>
    public class SubscriptionTypeDTO
    {
        #region Properties

        /// <summary>
        ///   Subscription type identifier.
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        ///   Name of subscription type.
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        ///   Fee for subscription.
        /// </summary>
        [Required]
        public int Fee { get; set; }

        /// <summary>
        ///   Percentage of fee discount.
        /// </summary>
        [Required]
        [Range(0, 100, ErrorMessage = "Percentage of subscription discount must be between 0% an 100%")]
        public int DiscountPercentage { get; set; }

        /// <summary>
        ///   Duration of subscription in months.
        /// </summary>
        [Required]
        [Range(1, 12, ErrorMessage = "Duration of subscription must be between 1 an 12 months")]
        public int DurationInMonths { get; set; }

        #endregion
    }
}