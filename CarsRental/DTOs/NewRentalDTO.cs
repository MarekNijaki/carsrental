﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarsRental.DTOs
{
    /// <summary>
    ///   New rental.
    /// </summary>
    public class NewRentalDTO
    {
        #region Properties

        /// <summary>
        ///   Customer identifier.
        /// </summary>
        [Required(ErrorMessage = "Identifier of customer")]
        public int CustomerId { get; set; }

        /// <summary>
        ///   List of car identifiers.
        /// </summary>
        public List<int> CarIds { get; set; }

        #endregion
    }
}