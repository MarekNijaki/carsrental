﻿using System.ComponentModel.DataAnnotations;

namespace CarsRental.DTOs
{
    /// <summary>
    ///   Car type.
    /// </summary>
    public class CarTypeDTO
    {
        #region Properties

        /// <summary>
        ///   Car type identifier.
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        ///   Name of the car type.
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        #endregion
    }
}