﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarsRental.DTOs
{
    /// <summary>
    ///   Customer DTO class.
    /// </summary>
    public class CustomerDTO
    {
        #region Properties

        /// <summary>
        ///   Customer identifier.
        /// </summary>
        [Required(ErrorMessage = "Identifier of customer is required")]
        public int Id { get; set; }

        /// <summary>
        ///   Name of the customer.
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        ///   Pesel.
        /// </summary>
        [Required]
        [StringLength(11)]
        public string Pesel { get; set; }

        /// <summary>
        ///   Birth date of customer.
        /// </summary>
        [Validation.Min18YearsDTO]
        public DateTime? Birthdate { get; set; }

        /// <summary>
        ///   Date of start rental.
        /// </summary>
        [Required]
        public DateTime RentalStartDate { get; set; }

        /// <summary>
        ///   Date of end rental.
        /// </summary>
        [Required]
        public DateTime RentalEndDate { get; set; }

        /// <summary>
        ///   Flag if customer is subscribed to some license (eg. yearly, monthly, etc).
        /// </summary>
        [Required]
        public bool IsSubscribed { get; set; }

        /// <summary>
        ///   Type of subscription.
        /// </summary>
        public SubscriptionTypeDTO SubscriptionType { get; set; }

        /// <summary>
        ///   Id of type of subscription.
        /// </summary>
        [Required]
        public int SubscriptionTypeId { get; set; }

        #endregion
    }
}