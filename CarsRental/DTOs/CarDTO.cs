﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarsRental.DTOs
{
    /// <summary>
    ///   Car class.
    /// </summary>
    public class CarDTO
    {
        #region Properties

        /// <summary>
        ///   Car identifier.
        /// </summary>
        [Required(ErrorMessage = "Identifier of car is required")]
        public int Id { get; set; }

        /// <summary>
        ///   Name of the car.
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        ///   Registration number.
        /// </summary>
        [Required]
        [MinLength(6)]
        [MaxLength(10)]
        public string RegistrationNumber { get; set; }

        /// <summary>
        ///   Car type.
        /// </summary>
        public CarTypeDTO CarType { get; set; }

        /// <summary>
        ///   Car type ID.
        /// </summary>
        [Required]
        public int CarTypeId { get; set; }

        /// <summary>
        ///   Date of production.
        /// </summary>
        [Required]
        public DateTime ProductionDate { get; set; }

        /// <summary>
        ///   Number of cars in stock.
        /// </summary>
        [Required]
        [Range(1, 50)]
        public byte NumberInStock { get; set; }

        /// <summary>
        ///   Number of available cars.
        /// </summary>
        [Required]
        [Range(0, 50)]
        public byte NumberAvailable { get; set; }

        #endregion
    }
}