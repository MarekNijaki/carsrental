﻿using CarsRental.DTOs;
using System;
using System.ComponentModel.DataAnnotations;

namespace CarsRental.Validation
{
    /// <summary>
    ///   Validation for customer age.
    /// </summary>
    public class Min18YearsDTO : ValidationAttribute
    {
        /// <summary>
        ///   Validation.
        /// </summary>
        /// <param name="value">Value of object</param>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Return flag if object is valid</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var customer = (CustomerDTO)validationContext.ObjectInstance;
            if (customer.Birthdate == null)
            {
                return ValidationResult.Success;
            }

            var age = DateTime.Today.Year - customer.Birthdate.Value.Year;

            return ((age >= 18) ? ValidationResult.Success : new ValidationResult("Customer mus be at least 18 years old"));
        }
    }
}