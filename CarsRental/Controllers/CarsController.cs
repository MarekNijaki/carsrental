﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using CarsRental.MixedModels;
using CarsRental.Models;

namespace CarsRental.Controllers
{
    /// <summary>
    ///   Cars controller.
    /// </summary>
    public class CarsController : Controller
    {
        #region Protected and private fields

        /// <summary>
        ///   Database context.
        /// </summary>
        private readonly ApplicationDbContext ctx;

        #endregion

        #region Public methods

        /// <summary>
        ///   Constructor.
        /// </summary>
        public CarsController()
        {
            ctx = new ApplicationDbContext();
        }

        /// <summary>
        ///   Return view with list of cars.
        ///   GET: Cars
        /// </summary>
        /// <returns>View with list of cars</returns>
        public ActionResult Index()
        {
            if (User.IsInRole(UserRole.Admin))
            {
                return View("Index");
            }
            else
            {
                return View("ReadOnlyIndex");
            }
        }

        /// <summary>
        ///   Return view with information about given car.
        /// </summary>
        /// <param name="id">Id of car</param>
        /// <returns>View with information about given car</returns>
        public ActionResult Info(int id)
        {
            Car car = ctx.Cars.Include(c => c.CarType).SingleOrDefault(c => c.Id == id);
            if (car == null)
            {
                return HttpNotFound();
            }

            return View(car);
        }

        /// <summary>
        ///   Create new car.
        /// </summary>
        /// <returns>View with form for creating new car</returns>
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult Create()
        {
            var mixedModel = new CreateOrEditCar
            {
                CarTypes = ctx.CarTypes.ToList()
            };

            return View("CreateOrEdit", mixedModel);
        }

        /// <summary>
        ///   Request for creating new car.
        /// </summary>
        /// <param name="car">Car data to save</param>
        /// <returns>View with list of cars</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult CreateRequest(Car car)
        {
            if (!ModelState.IsValid)
            {
                var mixedModel = new CreateOrEditCar
                {
                    CarTypes = ctx.CarTypes.ToList()
                };

                return View("CreateOrEdit", mixedModel);
            }

            // Update number of aviables cars.
            car.NumberAvailable = car.NumberInStock;

            ctx.Cars.Add(car);
            ctx.SaveChanges();

            return RedirectToAction("Index", "Cars");
        }

        /// <summary>
        ///   Edit car of given ID.
        /// </summary>
        /// <param name="id">Id of car to edit</param>
        /// <returns>View with car edit form</returns>
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult Edit(int id)
        {
            var car = ctx.Cars.SingleOrDefault(c => c.Id == id);
            if (car == null)
            {
                return HttpNotFound();
            }

            var mixedModel = new CreateOrEditCar()
            {
                Car = car,
                CarTypes = ctx.CarTypes.ToList()
            };

            return View("CreateOrEdit", mixedModel);
        }

        /// <summary>
        ///   Request for editing car.
        /// </summary>
        /// <param name="car">Car data to save</param>
        /// <returns>View with list of cars</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult EditRequest(Car car)
        {
            if (!ModelState.IsValid)
            {
                var mixedModel = new CreateOrEditCar()
                {
                    Car = car,
                    CarTypes = ctx.CarTypes.ToList()
                };

                return View("CreateOrEdit", mixedModel);
            }

            Car editedCar = ctx.Cars.Single(c => c.Id == car.Id);
            editedCar.Update(car);

            ctx.SaveChanges();

            return RedirectToAction("Index", "Cars");
        }

        #endregion

        #region Lifecycle methods

        /// <summary>
        ///   Dispose.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            ctx.Dispose();
            base.Dispose(disposing);
        }

        #endregion





        /*
        
        TESTING CODE:

        /// <summary>
        ///   GET: Cars/Random.
        /// </summary>
        /// <returns>Random car for tests</returns>
        public ActionResult Random()
        {
            var car = new Car() { Name = "Piaggo" };

            //return View(car);

            //return Content("some string to present on website...");

            //return HttpNotFound();

            //return new EmptyResult();

            //return RedirectToAction("Index", "Home", new { page = 1, sortBy = "name" });

            // 'Car' class object (data) will be passed to associated view (in the background it will be assigned to '...ViewResault.ViewData.Model').
            return View(car);
        }

        /// <summary>
        ///   Return view with list of cars.
        ///   GET: Cars
        ///   Example of action with two optional parameters.
        ///   Example usage: '/cars?pageIndex=2&orderBy=%22asd%22'
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="orderBy">Name of parameter to order by</param>
        /// <returns>View with list of cars</returns>
        public ActionResult Index(int? pageIndex, string orderBy)
        {
            if(!pageIndex.HasValue)
            {
                pageIndex = 1;
            }

            if(string.IsNullOrWhiteSpace(orderBy))
            {
                orderBy = "Name";
            }

            return Content("pageIndex=" + pageIndex + " orderBy="+ orderBy);
        }

        /// <summary>
        ///   Edit car of passed 'id'.
        ///   Parameter 'id' is embedded in URL.
        ///   In 'RoutConfig' there is default 'id' parameter name.
        ///   This could be changed to for example 'CarId'.
        ///   Example sage in URL: '/cars/edit/2' or '/cars/edit?id=2'.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            return Content("id=" + id);
        }

        /// <summary>
        ///   Cars produced on given date.
        ///   In 'RoutConfig' there is new route called 'productionDate'.
        ///   Example ussage: '/cars/productionDate/1994/11'.
        /// </summary>
        /// <param name="year">Year of production</param>
        /// <param name="month">Month of production</param>
        /// <returns>Cars produced on given date</returns>
        public ActionResult ByProductionDate(int year, int month)
        {
            return Content("year=" + year + " month=" + month);
        }

        /// <summary>
        ///   Cars produced after 2000 date.
        /// </summary>
        /// <param name="year">Year of production</param>
        /// <param name="month">Month of production</param>
        /// <returns>Cars produced after 2000 date</returns>
        public ActionResult ProducedAfter2000(int year, int month)
        {
            return Content("cars produced after 2000");
        }

        /// <summary>
        ///   Cars produced on given date.
        ///   Same as above but with custom ruting inside attribute.
        /// </summary>
        /// <param name="year">Year of production</param>
        /// <param name="month">Month of production</param>
        /// <returns>Cars produced on given date</returns>
        [Route("cars/productionDate/{year}/{month:regex(\\d{2}):range(1,12)}")]
        public ActionResult ByProductionDate(int year, int month)
        {
            return Content("year=" + year + " month=" + month);
        }

        /// <summary>
        ///   Get fake list of cars.
        /// </summary>
        /// <returns>Fake list of cars</returns>
        private IEnumerable<Car> GetFakeCars()
        {
            return new List<Car>
            {
                new Car { Id = 1, Name = "Polonez" },
                new Car { Id = 2, Name = "Fiat 126P" },
                new Car { Id = 3, Name = "Syrenka" }
            };
        }
        */
    }
}