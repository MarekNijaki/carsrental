﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using CarsRental.MixedModels;
using CarsRental.Models;

namespace CarsRental.Controllers
{
    /// <summary>
    ///   Customers controller.
    /// </summary>
    public class CustomersController : Controller
    {
        #region Protected and private fields

        /// <summary>
        ///   Database context.
        /// </summary>
        private readonly ApplicationDbContext ctx;

        #endregion

        #region Public methods

        /// <summary>
        ///   Constructor.
        /// </summary>
        public CustomersController()
        {
            ctx = new ApplicationDbContext();
        }

        /// <summary>
        ///   Return view with list of customers.
        ///   GET: Customers
        /// </summary>
        /// <returns>View with list of customers</returns>
        public ActionResult Index()
        {
            if (User.IsInRole(UserRole.Admin))
            {
                return View("Index");
            }
            else
            {
                return View("ReadOnlyIndex");
            }
        }

        /// <summary>
        ///   Return view with information about given customer.
        /// </summary>
        /// <param name="id">Id of customer</param>
        /// <returns>View with information about given customer</returns>
        public ActionResult Info(int id)
        {
            Customer customer = ctx.Customers.Include(c => c.SubscriptionType).SingleOrDefault(c => c.Id == id);
            if (customer == null)
            {
                return HttpNotFound();
            }

            return View(customer);
        }

        /// <summary>
        ///   Create new customer.
        /// </summary>
        /// <returns>View with form for creating new customer</returns>
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult Create()
        {
            var mixedModel = new CreateOrEditCustomer
            {
                SubscriptionTypes = ctx.SubscriptionTypes.ToList()
            };

            return View("CreateOrEdit", mixedModel);
        }

        /// <summary>
        ///   Request for creating new customer.
        /// </summary>
        /// <param name="customer">Customer data to save</param>
        /// <returns>View with list of customers</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult CreateRequest(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                var mixedModel = new CreateOrEditCustomer
                {
                    SubscriptionTypes = ctx.SubscriptionTypes.ToList()
                };

                return View("CreateOrEdit", mixedModel);
            }

            ctx.Customers.Add(customer);
            ctx.SaveChanges();

            return RedirectToAction("Index", "Customers");
        }

        /// <summary>
        ///   Edit customer of given ID.
        /// </summary>
        /// <param name="id">Id of customer to edit</param>
        /// <returns>View with customer edit form</returns>
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult Edit(int id)
        {
            var customer = ctx.Customers.SingleOrDefault(c => c.Id == id);
            if (customer == null)
            {
                return HttpNotFound();
            }

            var mixedModel = new CreateOrEditCustomer()
            {
                Customer = customer,
                SubscriptionTypes = ctx.SubscriptionTypes.ToList()
            };

            return View("CreateOrEdit", mixedModel);
        }

        /// <summary>
        ///   Request for editing customer.
        /// </summary>
        /// <param name="customer">Customer data to save</param>
        /// <returns>View with list of customers</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult EditRequest(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                var mixedModel = new CreateOrEditCustomer()
                {
                    Customer = customer,
                    SubscriptionTypes = ctx.SubscriptionTypes.ToList()
                };

                return View("CreateOrEdit", mixedModel);
            }

            Customer editedCustomer = ctx.Customers.Single(c => c.Id == customer.Id);
            editedCustomer.Update(customer);

            ctx.SaveChanges();

            return RedirectToAction("Index", "Customers");
        }

        #endregion

        #region Lifecycle methods

        /// <summary>
        ///   Dispose.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            ctx.Dispose();
            base.Dispose(disposing);
        }

        #endregion
    }
}