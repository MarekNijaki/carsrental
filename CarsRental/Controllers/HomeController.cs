﻿using System.Web.Mvc;

namespace CarsRental.Controllers
{
    /// <summary>
    ///   Home page.
    ///   Anonymous user are allowed to view this page.
    /// </summary>
    [AllowAnonymous]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}