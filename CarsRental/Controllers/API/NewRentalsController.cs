﻿using CarsRental.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using CarsRental.DTOs;
using AutoMapper;
using System;

namespace CarsRental.Controllers.API
{
    /// <summary>
    ///   New rentals controller.
    /// </summary>
    public class NewRentalsController : ApiController
    {
        #region Protected and private fields

        /// <summary>
        ///   Database context.
        /// </summary>
        private readonly ApplicationDbContext ctx;

        #endregion

        #region Public methods

        /// <summary>
        ///   Constructor.
        /// </summary>
        public NewRentalsController()
        {
            ctx = new ApplicationDbContext();
        }

        /// <summary>
        ///   Create rentals (eg. POST /API/rental).
        /// </summary>
        /// <param name="newRentalDTO">Rental to create</param>
        /// <returns>Created rental</returns>
        [HttpPost]
        [Authorize(Roles = UserRole.Admin)]
        public IHttpActionResult CreateRentals(NewRentalDTO newRentalDTO)
        {
            Customer customer = ctx.Customers.Single(c => c.Id == newRentalDTO.CustomerId);

            List<Car> cars = ctx.Cars.Where(c => newRentalDTO.CarIds.Contains(c.Id)).ToList();

            foreach (var car in cars)
            {
                if (car.NumberAvailable == 0)
                {
                    return BadRequest("This car is not available");
                }

                car.NumberAvailable--;

                var rental = new Rental
                {
                    Customer = customer,
                    Car = car,
                    DateStart = DateTime.Now
                };

                ctx.Rentals.Add(rental);
            }

            ctx.SaveChanges();

            return Ok();
        }

        #endregion

        #region Lifecycle methods

        /// <summary>
        ///   Dispose.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            ctx.Dispose();
            base.Dispose(disposing);
        }

        #endregion
    }
}