﻿using CarsRental.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using CarsRental.DTOs;
using AutoMapper;
using System;

namespace CarsRental.Controllers.API
{
    /// <summary>
    ///   Cars controller.
    /// </summary>
    public class CarsController : ApiController
    {
        #region Protected and private fields

        /// <summary>
        ///   Database context.
        /// </summary>
        private readonly ApplicationDbContext ctx;

        #endregion

        #region Public methods

        /// <summary>
        ///   Constructor.
        /// </summary>
        public CarsController()
        {
            ctx = new ApplicationDbContext();
        }

        /// <summary>
        ///   Get cars (eg. GET /API/cars).
        /// </summary>
        /// <returns>Cars</returns>
        [HttpGet]
        public IHttpActionResult GetCars(string query = null)
        {
            var cars = ctx.Cars.Include(c => c.CarType).Where(c => c.NumberAvailable > 0);

            if (!String.IsNullOrWhiteSpace(query))
            {
                cars = cars.Where(c => c.Name.Contains(query));
            }

            var carsDTO = cars.ToList().Select(Mapper.Map<Car, CarDTO>);

            return Ok(carsDTO);
        }

        /// <summary>
        ///   Get car (eg. GET /API/cars/1)
        /// </summary>
        /// <param name="id">Id of car to get</param>
        /// <returns>Car</returns>
        [HttpGet]
        public IHttpActionResult GetCar(int id)
        {
            Car car = ctx.Cars.SingleOrDefault(c => c.Id == id);
            if (car == null)
            {
                return NotFound();
            }


            return Ok(Mapper.Map<Car, CarDTO>(car));
        }

        /// <summary>
        ///   Create car (eg. POST /API/cars).
        /// </summary>
        /// <param name="carDTO">Car to create</param>
        /// <returns>Created car</returns>
        [HttpPost]
        [Authorize(Roles = UserRole.Admin)]
        public IHttpActionResult CreateCar(CarDTO carDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Car car = Mapper.Map<CarDTO, Car>(carDTO);

            // Update number of aviables cars.
            carDTO.NumberAvailable = carDTO.NumberInStock;

            ctx.Cars.Add(car);
            ctx.SaveChanges();

            // Save ID generated in database to DTO object.
            carDTO.Id = car.Id;

            // URI - unified resource identifier (eg. /API/cars/10).
            return Created(new Uri(Request.RequestUri + "/" + car.Id), carDTO);
        }

        /// <summary>
        ///   Edit car (eg. PUT /API/cars/1).
        /// </summary>
        /// <param name="id">Id of car to edit</param>
        /// <param name="carDTO">Edited car</param>
        [HttpPut]
        [Authorize(Roles = UserRole.Admin)]
        public IHttpActionResult EditCar(int id, CarDTO carDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Car car = ctx.Cars.SingleOrDefault(c => c.Id == id);
            if (car == null)
            {
                return NotFound();
            }

            Mapper.Map<CarDTO, Car>(carDTO, car);

            ctx.SaveChanges();

            return Ok();
        }

        /// <summary>
        ///   Remove given car (eg. DELETE /API/car/1).
        /// </summary>
        /// <param name="id">Id of car to remove</param>
        [HttpDelete]
        [Authorize(Roles = UserRole.Admin)]
        public IHttpActionResult RemoveCar(int id)
        {
            Car car = ctx.Cars.SingleOrDefault(c => c.Id == id);
            if (car == null)
            {
                return NotFound();
            }

            ctx.Cars.Remove(car);
            ctx.SaveChanges();

            return Ok();
        }

        #endregion

        #region Lifecycle methods

        /// <summary>
        ///   Dispose.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            ctx.Dispose();
            base.Dispose(disposing);
        }

        #endregion
    }
}
