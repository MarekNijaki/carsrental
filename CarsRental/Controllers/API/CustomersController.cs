﻿using CarsRental.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using CarsRental.DTOs;
using AutoMapper;
using System;

namespace CarsRental.Controllers.API
{
    /// <summary>
    ///   Customers controller.
    /// </summary>
    public class CustomersController : ApiController
    {
        #region Protected and private fields

        /// <summary>
        ///   Database context.
        /// </summary>
        private readonly ApplicationDbContext ctx;

        #endregion

        #region Public methods

        /// <summary>
        ///   Constructor.
        /// </summary>
        public CustomersController()
        {
            ctx = new ApplicationDbContext();
        }

        /// <summary>
        ///   Get customers (eg. GET /API/customers).
        /// </summary>
        /// <returns>Customers</returns>
        [HttpGet]
        public IHttpActionResult GetCustomers(string query = null)
        {
            var customers = ctx.Customers.Include(c => c.SubscriptionType);

            if (!String.IsNullOrWhiteSpace(query))
            {
                customers = customers.Where(c => c.Name.Contains(query));
            }

            var customersDTO = customers.ToList().Select(Mapper.Map<Customer, CustomerDTO>);

            return Ok(customersDTO);
        }

        /// <summary>
        ///   Get customer (eg. GET /API/customers/1)
        /// </summary>
        /// <param name="id">Id of customer to get</param>
        /// <returns>Customer</returns>
        [HttpGet]
        public IHttpActionResult GetCustomer(int id)
        {
            Customer customer = ctx.Customers.SingleOrDefault(c => c.Id == id);
            if (customer == null)
            {
                return NotFound();
            }


            return Ok(Mapper.Map<Customer, CustomerDTO>(customer));
        }

        /// <summary>
        ///   Create customer (eg. POST /API/customers).
        /// </summary>
        /// <param name="customerDTO">Customer to create</param>
        /// <returns>Created customer</returns>
        [HttpPost]
        [Authorize(Roles = UserRole.Admin)]
        public IHttpActionResult CreateCustomer(CustomerDTO customerDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Customer customer = Mapper.Map<CustomerDTO, Customer>(customerDTO);

            ctx.Customers.Add(customer);
            ctx.SaveChanges();

            // Save ID generated in database to DTO object.
            customerDTO.Id = customer.Id;

            // URI - unified resource identifier (eg. /API/customers/10).
            return Created(new Uri(Request.RequestUri + "/" + customer.Id), customerDTO);
        }

        /// <summary>
        ///   Edit customer (eg. PUT /API/customers/1).
        /// </summary>
        /// <param name="id">Id of customer to edit</param>
        /// <param name="customerDTO">Edited customer</param>
        [HttpPut]
        [Authorize(Roles = UserRole.Admin)]
        public IHttpActionResult EditCustomer(int id, CustomerDTO customerDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Customer customer = ctx.Customers.SingleOrDefault(c => c.Id == id);
            if (customer == null)
            {
                return NotFound();
            }

            Mapper.Map<CustomerDTO, Customer>(customerDTO, customer);

            ctx.SaveChanges();

            return Ok();
        }

        /// <summary>
        ///   Remove given customer (eg. DELETE /API/customer/1).
        /// </summary>
        /// <param name="id">Id of customer to remove</param>
        [HttpDelete]
        [Authorize(Roles = UserRole.Admin)]
        public IHttpActionResult RemoveCustomer(int id)
        {
            Customer customer = ctx.Customers.SingleOrDefault(c => c.Id == id);
            if (customer == null)
            {
                return NotFound();
            }

            ctx.Customers.Remove(customer);
            ctx.SaveChanges();

            return Ok();
        }

        #endregion

        #region Lifecycle methods

        /// <summary>
        ///   Dispose.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            ctx.Dispose();
            base.Dispose(disposing);
        }

        #endregion
    }
}
