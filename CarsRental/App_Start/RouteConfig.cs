﻿using System.Web.Mvc;
using System.Web.Routing;

namespace CarsRental
{
    /// <summary>
    ///   Routes configuration.
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        ///   Routes registration.
        ///   Order of routes will influence routes configuration.
        /// </summary>
        /// <param name="routes">Collection of routes</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Enable attribute routing in actions.
            routes.MapMvcAttributeRoutes();

            /* 
            routes.MapRoute(
                name: "CarsProducedAfter2000",
                url: "cars/producedAfter2000/{year}/{month}",
                defaults: new { controller = "Cars", action = "ProducedAfter2000" },
                          new { year = @"2\d{3}", month = @"\d{2}" }
            );

            routes.MapRoute(
                name: "CarsByProductionDate",
                url: "cars/productionDate/{year}/{month}",
                defaults: new { controller = "Cars", action = "ByProductionDate" },
                          new { year = @"\d{4}", month = @"\d{2}" }
            );
            */

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
