﻿using AutoMapper;
using CarsRental.DTOs;
using CarsRental.Models;

namespace CarsRental.App_Start
{
    /// <summary>
    ///   Map domain objects to DTOs.
    /// </summary>
    public class MappingProfile : Profile
    {
        #region Public methods

        /// <summary>
        ///   Constructor.
        /// </summary>
        public MappingProfile()
        {
            // Domain data to DTO data.
            Mapper.CreateMap<Customer, CustomerDTO>();
            Mapper.CreateMap<SubscriptionType, SubscriptionTypeDTO>();
            Mapper.CreateMap<Car, CarDTO>();
            Mapper.CreateMap<CarType, CarTypeDTO>();


            // DTO data to domain data (ingnore ID - it will be autocreated).
            Mapper.CreateMap<CustomerDTO, Customer>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<CarDTO, Car>().ForMember(c => c.Id, opt => opt.Ignore());
        }

        #endregion
    }
}