﻿namespace CarsRental.Models
{
    /// <summary>
    ///   User role.
    /// </summary>
    public class UserRole
    {
        #region Public fields

        /// <summary>
        ///   Admin role.
        /// </summary>
        public const string Admin = "Admin";

        #endregion
    }
}