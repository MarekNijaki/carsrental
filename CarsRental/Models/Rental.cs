﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarsRental.Models
{
    /// <summary>
    ///   Rental class.
    /// </summary>
    public class Rental
    {
        #region Properties

        /// <summary>
        ///   Rental identifier.
        /// </summary>
        [Required(ErrorMessage = "Identifier of rental is required")]
        public int Id { get; set; }

        /// <summary>
        ///   Customer associated with rental.
        /// </summary>
        [Required]
        public Customer Customer { get; set; }

        /// <summary>
        ///   Car associated with rental.
        /// </summary>
        [Required]
        public Car Car { get; set; }

        /// <summary>
        ///   Start date of rental.
        /// </summary>
        public DateTime DateStart { get; set; }

        /// <summary>
        ///   End date of rental.
        /// </summary>
        public DateTime? DateEnd { get; set; }

        #endregion
    }
}