﻿using CarsRental.Validation;
using System;
using System.ComponentModel.DataAnnotations;

namespace CarsRental.Models
{
    /// <summary>
    ///   Customer class.
    /// </summary>
    public class Customer
    {
        #region Properties

        /// <summary>
        ///   Customer identifier.
        /// </summary>
        [Required(ErrorMessage = "Identifier of customer is required")]
        public int Id { get; set; }

        /// <summary>
        ///   Name of the customer.
        /// </summary>
        [Required]
        [StringLength(50)]
        [Display(Name = "Name:")]
        public string Name { get; set; }

        /// <summary>
        ///   Pesel.
        /// </summary>
        [Required]
        [StringLength(11)]
        [Display(Name = "Pesel:")]
        public string Pesel { get; set; }

        /// <summary>
        ///   Birth date of customer.
        /// </summary>
        [Display(Name = "Date of birth:")]
        [Min18Years]
        public DateTime? Birthdate { get; set; }

        /// <summary>
        ///   Date of start rental.
        /// </summary>
        [Required]
        [Display(Name = "Rental start date:")]
        public DateTime RentalStartDate { get; set; }

        /// <summary>
        ///   Date of end rental.
        /// </summary>
        [Required]
        [Display(Name = "Rental end date:")]
        public DateTime RentalEndDate { get; set; }

        /// <summary>
        ///   Flag if customer is subscribed to some license (eg. yearly, monthly, etc).
        /// </summary>
        [Required]
        [Display(Name = "Is subscribed:")]
        public bool IsSubscribed { get; set; }

        /// <summary>
        ///   Type of subscription.
        /// </summary>
        [Display(Name = "Subscription type:")]
        public SubscriptionType SubscriptionType { get; set; }

        /// <summary>
        ///   Id of type of subscription.
        /// </summary>
        [Required]
        [Display(Name = "Subscription type:")]
        public int SubscriptionTypeId { get; set; }

        #endregion

        #region Public methods

        /// <summary>
        ///   Update customer.
        /// </summary>
        /// <param name="customer">Updated customer</param>
        public void Update(Customer customer)
        {
            Name = customer.Name;
            Pesel = customer.Pesel;
            Birthdate = customer.Birthdate;
            RentalStartDate = customer.RentalStartDate;
            RentalEndDate = customer.RentalEndDate;
            IsSubscribed = customer.IsSubscribed;
            SubscriptionTypeId = customer.SubscriptionTypeId;
        }

        #endregion
    }
}