﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarsRental.Models
{
    /// <summary>
    ///   Car class.
    /// </summary>
    public class Car
    {
        #region Properties

        /// <summary>
        ///   Car identifier.
        /// </summary>
        [Required(ErrorMessage = "Identifier of car is required")]
        public int Id { get; set; }

        /// <summary>
        ///   Name of the car.
        /// </summary>
        [Required]
        [StringLength(50)]
        [Display(Name = "Name:")]
        public string Name { get; set; }

        /// <summary>
        ///   Registration number.
        /// </summary>
        [Required]
        [MinLength(6)]
        [MaxLength(10)]
        [Display(Name = "Registration number:")]
        public string RegistrationNumber { get; set; }

        /// <summary>
        ///   Car type.
        /// </summary>
        [Display(Name = "Type:")]
        public CarType CarType { get; set; }

        /// <summary>
        ///   Car type ID.
        /// </summary>
        [Required]
        [Display(Name = "Type:")]
        public int CarTypeId { get; set; }

        /// <summary>
        ///   Date of production.
        /// </summary>
        [Required]
        [Display(Name = "Production date:")]
        public DateTime ProductionDate { get; set; }

        /// <summary>
        ///   Number of cars in stock.
        /// </summary>
        [Required]
        [Range(1,50)]
        [Display(Name = "Number in stock:")]
        public byte NumberInStock { get; set; }

        /// <summary>
        ///   Number of available cars.
        /// </summary>
        [Required]
        [Range(0, 50)]
        [Display(Name = "Number of available cars:")]
        public byte NumberAvailable { get; set; }

        #endregion

        #region Public methods

        /// <summary>
        ///   Update car.
        /// </summary>
        /// <param name="car">Updated car</param>
        public void Update(Car car)
        {
            Name = car.Name;
            RegistrationNumber = car.RegistrationNumber;
            CarTypeId = car.CarTypeId;
            ProductionDate = car.ProductionDate;
            NumberInStock = car.NumberInStock;
        }

        #endregion
    }
}