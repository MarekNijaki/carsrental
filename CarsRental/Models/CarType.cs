﻿using System.ComponentModel.DataAnnotations;

namespace CarsRental.Models
{
    /// <summary>
    ///   Car type.
    /// </summary>
    public class CarType
    {
        #region Properties

        /// <summary>
        ///   Car type identifier.
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        ///   Name of the car type.
        /// </summary>
        [Required]
        [StringLength(50)]
        [Display(Name = "Car type:")]
        public string Name { get; set; }

        #endregion
    }
}